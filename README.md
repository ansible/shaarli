# Shaarli

Ansible role to install, configure and backup/restore Shaarli.
This role also installs and enables Shaarli plugins.

To update Shaarli itself, re-run this role with `shaarli.version` set to
`latest` or to a version string more recent than the one installed.

To update Shaarli's external plugins, simply re-running this role as-is
should suffice (assuming there are new versions available upstream, and that
you have not changed `version` for each plugin from its default value).


## shaarli2mastodon plugin (Shaarli -> Mastodon)

See [your own notes](https://links.solarchemist.se/shaare/YSJ_Cg) and
[shaarli2mastodon's README](https://github.com/kalvn/shaarli2mastodon#readme).


## Migrated from dahab to damietta

Shaarli used to live on dahab, were it shared the VPS with Gitea and a mailserver.
In order to give the mailserver a dedicated VPS, I decided to migrate Shaarli
to damietta.
This will also allow us to update PHP more often, which Shaarli benefits from.


## Refs

+ https://github.com/shaarli/Shaarli
+ https://shaarli.readthedocs.io/en/master
+ https://github.com/ovv/ansible-role-shaarli
+ https://github.com/MicroJoe/ansible-role-shaarli
+ https://github.com/kalvn/shaarli2mastodon
