---

- name: Entered install.yml
  debug:
    msg: "Entered install.yml"
  when: shaarli_testing | bool

- debug:
    var: shaarli_version
  when: shaarli_testing | bool

- debug:
    var: shaarli_version_installed
  when: shaarli_testing | bool

# https://docs.ansible.com/ansible/latest/collections/ansible/builtin/get_url_module.html
- name: "Download Shaarli v{{ shaarli_version }} tarball"
  ansible.builtin.get_url:
    # https://github.com/shaarli/Shaarli/releases/download/v0.12.2/shaarli-v0.12.2-full.tar.gz
    url: "https://github.com/shaarli/Shaarli/releases/download/v{{ shaarli_version }}/shaarli-v{{ shaarli_version }}-full.tar.gz"
    dest: "{{ shaarli.path.tarball }}"
    force: true
    mode: u=rw,go=r

# note that if canary exists, shaarli.path.tarball is "skipped: true", and
# does not contain any dict "dest"
- debug:
    var: shaarli.path.tarball
  when: shaarli_testing | bool

# note, there is no need to wipe this directory before unpacking the archive
# in fact, unpacking in place makes updates easier (seems to work OK)
- name: "Make sure directory {{ shaarli.path.gitrepo }} exists"
  ansible.builtin.file:
    path: "{{ shaarli.path.gitrepo }}"
    state: directory
    owner: "{{ shaarli.user }}"
    group: "{{ shaarli.user }}"

- name: "Unpack Shaarli to {{ shaarli.path.gitrepo }}"
  ansible.builtin.unarchive:
    remote_src: yes
    src: "{{ shaarli.path.tarball }}"
    dest: "{{ shaarli.path.gitrepo }}"
    owner: "{{ shaarli.user }}"
    group: "{{ shaarli.user }}"
    # strip the unnecessary top directory "Shaarli" inside the tarball
    extra_opts: "--strip-components=1"

- name: Make sure our "canary" is created (only shaarli_testing)
  ansible.builtin.file:
    path: "{{ shaarli.path.gitrepo }}/data/datastore.php"
    state: touch
    owner: "{{ shaarli.user }}"
    group: "{{ shaarli.user }}"
  when: shaarli_testing | bool


- name: Install plugins
  block:

    # necessary because git plugin (see below) requires empty destination directory
    - name: Wipe any existing plugin directories
      ansible.builtin.file:
        path: "{{ shaarli.path.gitrepo }}/plugins/{{ item.name }}"
        state: absent
      loop: "{{ shaarli_plugins.install }}"

    # This task may fail on a fresh provision:
    #   /usr/bin/git clone --origin origin https://github.com/kalvn/shaarli2mastodon /home/taha/public/shaarli/plugins/shaarli2mastodon
    #   Fatal: could not create leading directories of '/home/taha/public/shaarli/plugins/shaarli2mastodon':
    #   Permission denied
    - name: Install all Shaarli plugins as {{ shaarli.user }}
      # https://docs.ansible.com/ansible/latest/collections/ansible/builtin/git_module.html
      # git plugin does not have any owner/group attribute
      ansible.builtin.git:
        repo: "{{ item.repo }}"
        dest: "{{ shaarli.path.gitrepo }}/plugins/{{ item.name }}"
        version: "{{ item.version }}"
      become: true
      become_user: "{{ shaarli.user }}"
      loop: "{{ shaarli_plugins.install }}"

  rescue:

    # Running this task as ansible_env.USER fails with:
    # fatal: could not create work tree dir: Permission denied
    # Therefore we run this task as root!
    - name: Install all Shaarli plugins
      ansible.builtin.git:
        repo: "{{ item.repo }}"
        dest: "{{ shaarli.path.gitrepo }}/plugins/{{ item.name }}"
        version: "{{ item.version }}"
      loop: "{{ shaarli_plugins.install }}"

    - name: Ensure created plugin directories are owned by {{ shaarli.user }}
      ansible.builtin.file:
        path: "{{ shaarli.path.gitrepo }}/plugins/{{ item.name }}"
        owner: "{{ shaarli.user }}"
        group: "{{ shaarli.user }}"
        recurse: yes
      loop: "{{ shaarli_plugins.install }}"
  # END OF BLOCK
