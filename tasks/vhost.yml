---

- name: "Check for existing Let's Encrypt certificate for {{ shaarli.domain }}"
  ansible.builtin.stat:
    path: "/etc/letsencrypt/live/{{ shaarli.domain }}/cert.pem"
  register: vhost_cert

# Unless you want your users to get scary certificate errors when attempting to
# visit using your old primary servername (now alias #1), we must fetch a cert
# containing *both* domains (we have to maintain certs for both domains because
# I have HSTS enabled).
# Do not deprecate the old primary domain until you are sure old links won't
# be broken (which is never).
- name: "Fetch TLS certificate for {{ shaarli.domain }} and {{ shaarli.aliases[0] }}"
  ansible.builtin.command: |
    certbot certonly --noninteractive
    --agree-tos --preferred-challenges=http
    --authenticator standalone
    --pre-hook "systemctl stop apache2.service"
    --post-hook "systemctl start apache2.service"
    --email {{ shaarli.email }}
    -d {{ shaarli.domain }}
    -d {{ shaarli.aliases[0] }}
  when: not (vhost_cert.stat.exists | bool)
  tags: certbot

# https://shaarli.readthedocs.io/en/master/Server-configuration/#examples
# this template uses trim_blocks: no, which is set inside the template file itself
- name: "Configure Apache vhost"
  ansible.builtin.template:
    src: "vhost.conf.j2"
    dest: "/etc/apache2/sites-available/{{ shaarli.domain }}.conf"
    owner: root
    group: root
    mode: u=rw,go=r
  notify: reload apache

- name: "Check whether vhost is already enabled"
  ansible.builtin.stat:
    path: "/etc/apache2/sites-enabled/{{ shaarli.domain }}.conf"
  register: vhost_enabled

- name: "a2ensite {{ shaarli.domain}}"
  ansible.builtin.command: "a2ensite {{ shaarli.domain }}.conf"
  when: not (vhost_enabled.stat.exists | bool)
  tags: certbot
  notify: reload apache
